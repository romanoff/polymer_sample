import 'package:polymer/polymer.dart';
import 'dart:async';
import 'dart:html';

@CustomTag('a-timer')
class CustomTimer extends PolymerElement {
  @observable int counter = 0;
  @published String textValue = 'Here is counter:';
  Timer timer;

  CustomTimer.created() : super.created() {
  }

  void start() {
    stop();
    this.timer = new Timer.periodic(new Duration(seconds: 1), update);
  }

  void update(e) {
    counter+= 1;
  }

  void stop() {
    if (this.timer != null) {
      this.timer.cancel();
    }
    counter = 0;
    dispatchEvent(new CustomEvent('counterstopped'));
  }

  @override
  void attached() {
    super.attached();
    // This method is called when custom element is attached to the DOM
    print("I am attached. Counter: ${counter}");
    var startButton = $['start'];
    print(startButton.firstChild.data);
    var text = $['text'];
    text.onClick.listen(update);
  }
  @override
  void detached() {
    super.detached();
    // Method is called after element is detached
  }

  @override
  void attributeChanged(String name, String oldValue, String newValue) {
    super.attributeChanged(name, oldValue, newValue);
    //Called when an attribute, such as class, of an instance of the custom element is added, changed, or removed
  }

}
