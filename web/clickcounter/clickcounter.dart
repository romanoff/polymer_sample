import 'package:polymer/polymer.dart';
// import '../timer/timer.dart';

/**
 * A Polymer click counter element.
 */
@CustomTag('click-counter')
class ClickCounter extends PolymerElement {
  @published int count = 0;

  ClickCounter.created() : super.created() {
  }

  void increment() {
    count+=2;
  }

  void doubleIncrement() {
    count += 4;
  }
  void makeMeHappy(){
    this.increment();
    print("Let's try to make you happy");
  }

  void doSomething(e) {
    print("Custom doSomething handler on clickcounter!");
  }

  @override
  void attached() {
    super.attached();
    var timer = $['timer'];
    timer.attributes['textValue'] = 'Text value set form attached handler';
    // ($['timer'] as CustomTimer).textValue = 'Text value set form attached handler';
  }
}
